# Cybersole

## OVERVIEW 总览
- 交易条件: 解绑后的Key绑定到你的Discord后就算交易成功
- 能否解绑: 能
- 原始价格: 300英镑
- 续订费用: 100英镑/半年
- 网站链接: https://cybersole.io/
- 官方推特: https://twitter.com/Cybersole
- 下载链接: https://cybersole.io/download
### 支持的商店
Cybersole 当前支持的商店如下：
``` Shopify
- Supreme (US, EU, JP)
- Supreme In Store Signups
- JD Group (Finishline, JD Sports US)
- Footsites (Footlocker US, Kids Footlocker, Footaction, Eastbay, Champs Sports, Footlocker CA)
- Shoe Palace
- Hibbett
- Footlocker EU
- Footlocker AU
- Footlocker SG, MO, MY
- Sidestep NL, DE
- Courir
- Mesh (Footpatrol, Size?, The Hip Store, JD Sports)
- Off---White
- MrPorter, Net-A-Porter
- Offspring
- Slam Jam
- Kicks Store
- Hot Topic
- Box Lunch
- Lacoste
```

### 补货
Cyber​​ AIO每月在随机的日期和时间进行一次补货，零售价为300英镑，并收取每6个月100英镑的续订费。想获得购买的机会，请关注他们的Twitter帐户和Instagram帐户，并确保开启了通知提醒。

### 如何安装Cybersole
**第一步** 下载安装程序   
- 安装方式一：https://cybersole.io/download   
- 安装方式二：进入控制版页面 https://cybersole.io/dashboard 点击下载按钮下载
![](https://support.cybersole.io/hc/article_attachments/360030860891/mceclip0.png)

**第二步** 输入您的许可证密钥（License Key），然后点击绿色按钮（Authenticate）
![](https://support.cybersole.io/hc/article_attachments/360030888072/mceclip0.png)

**第三步** Cyber​​AIO将启动下载并打开  
![](https://support.cybersole.io/hc/article_attachments/360023091391/mceclip3.png)

**第四步**Cyber​​AIO已成功安装！   
![](https://support.cybersole.io/hc/article_attachments/360030888092/mceclip1.png)


### bot布局
- 任务  
正确设置任务将确保机器人在正确的时间找到要购买的产品，点击左下角的创建任务（Create Tasks）  
任务包括：
```
    Site （网站）
    Checkout Profile （结账用的账单）
    Size （尺码）
    Keywords/Direct Link/SKU （关键词，链接，SKU（相当于产品的身份证））
    Amount [of tasks to create] （创建任务的数量）
    Start time（设置启动时间）
    Select proxy （使用的代理IP）
    Account （账号）
    Shipping Rate （邮寄费率）

```
![](https://support.cybersole.io/hc/article_attachments/360030888372/mceclip0.png)

这是一个任务创建窗口，您可以在其中设置任务。关于它们的作用，所有选项都一目了然
![](https://support.cybersole.io/hc/article_attachments/360030888392/mceclip1.png)
- 验证码队列  
打开验证码队列（Captcha Queue）时，您可以登录谷歌账号到每个解码器中，以帮助确保获得更多的一键式验证码（Oneclick）。
![](https://support.cybersole.io/hc/article_attachments/360030861131/mceclip2.png)

- 代理  
在此页面上，您会找到添加的代理列表。要加载代理，只需单击“添加代理（左上角的加号按钮）”，然后以所需格式输入即可。格式使用IP：Port：Username：Password或仅IP：Port。  
![](https://support.cybersole.io/hc/article_attachments/360030888452/mceclip3.png)
添加代理后，点击“测试所有代理 下方正中央（Test Proxies）”以确保它们正常工作。延迟（以毫秒为单位 ms）越低，代理越快。
- 账单地址  
在这里，你可以找到自己的结账资料。这里的所有数据都存储在本地，因此只有你可以访问。   
![](https://support.cybersole.io/hc/article_attachments/360030888472/mceclip4.png)
- 账号  
在帐户页面中，您可以为每个网站添加您的个人登录详细信息，例如Undefeated。
![](https://support.cybersole.io/hc/article_attachments/360030861331/mceclip0.png)

- 费率   
在费率页面中，您可以预加载运费，以便在发布期间更快地结帐！
![](https://support.cybersole.io/hc/article_attachments/360030861371/mceclip1.png)

- 设置   
此页面使您有机会重设（停用）密钥，更改延迟并手动检查更新。您还可以配置您的Webhook，当您成功签出或付款被拒时，机器人会发送成功日志。
![](https://support.cybersole.io/hc/article_attachments/360030888492/mceclip5.png)

# 各大网站设置
## shopify 设置
> 模式      

- **fast**   
 适用于没有机器人防御(bot protection)的网站,wishatl,oneness以及补货等

- **safe**   
适用于上密码页且有机器人防御(bot protection)的网站，如dsm，kaws等    

- **safe preload**   
适用于无密码页且有机器人防御(bot protection)的网站，如und，kith等

> 延迟
- **准点发售** : `1000-3000+ms`
- **补货** : `3000-5000+ms`  
*代理的数量决定了你应该运行的延迟，代理越多，延迟越低，代理越少，延迟越高。*

> 开始时间  
- 无密码页面在发售前2-5分钟开任务，有密码页面在发售前30秒，确保在无密码发售初期启动任务时，延迟时间在7500MS以上，然后在发售前30秒降低延迟时间。

> 代理  
有机器人防御(bot protection)
- 无流量盾(proxy protection)：dc/isp/local都可以  
- 有流量盾(proxy protection)：resi/破盾dc/local

## supreme 设置
> 模式：
- 准点发售时，你可能可以用fast模式完成一些任务，但大多数应该是sage模式。在补货时，请混合使用所有三种模式。

> 延迟  
- 发售 :` 200-800 ms `
- 补货 :` 200-1000+ms`    

代理的数量也决定了你应该运行的延迟，代理越多，延迟越低，代理越少，延迟越高。

> 开始时间
- 发售前30秒

> 代理  
- 强烈建议在Supreme上使用DCs而不是Resi。Resis也可以用，但会降低你被抓的机会。   
## 四大footsite 设置
> SKU:
- 在Footsite的发布页面(release page)中找到,有时候每个Footsite中的SKU会有所不同。

> 延迟  
- 发售：`500-3000+ms`
- 补货:`3500-4500+ms`
发售应低一些，如果补货打算长期运行，则应高一些。  
> 开始时间
- 发售前30-10分钟

> 代理
- 由于现在四大不再是akamai protection，改为datadome了，所以对ip的临时禁用更频繁了，所以推荐使用resi，如果你手头上大量的isp，也是可以用的，但是不推荐任务代理比例1比多，推荐任务代理比为多对1。


## FNL/JD US 设置
> SKU
- 在release页面中发现，与商品SKU不同

>颜色ID
- 在release页面中发现,是SKU的最后三个数字，例如：5550088-001，001是颜色ID。5550088-001，001是颜色ID。

> 延迟
- 发售和补货都请用`5000+ms`。网站ban ip很厉害，所以在发售前就开始任务，并使用高延迟。
> 开始时间
- 发售前1分钟

> 代理
- resi或者可以跑的dc/isp




