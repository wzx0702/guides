# Footsites(四大)

## footsites是什么
### footsites是Footlocker、Eastbay、Footaction、Champssports的统称，当然也包含kidsfootlocker、ladyfootlocker和footlocker ca等分站。 footlocker在全球范围各个地区都有相应的网站，一般我们按大洲分区。
``` 
footlocker au http://footlocker.com.au/ 
footlocker asia http://footlocker.sg /hk/my/mo
footlocker eu http://footlocker.it se/fr/de/co.uk等
```

## 为什么要了解四大
### 四大作为货量经次于Nike官网的网站，是必买的网站之一。在货量大的同时，其本身又对地址和卡要求相对较低（国卡转运即可购买）。

## 用什么bot跑四大
### 市面上现在四大多如牛毛，对于四大在不同情况下的bot选用情况是不一样的，也就是说你想全方位制霸，就需要有各种bot（武器库）。具体的场景可大致分为以下几种（不讨论proxy和小众bot，用户数少于200）
- 准点发售：基本所有能work的bot都能买。表现比较突出的有cyber，whatbot，prism，kodai，kylin，valor
- 准点发售后的补货:准点发售后的补货一般在1-3点左右，一般都是一点点的上库存，所以需要能堆任务和加车能力较强的bot，其中valor,prism,cyber，kylin等表现都不错
- #### 新品普货发售：
    > 1. 如果是先上了产品页没上库存的，whatbot，prism，nsb，kylin，balko，valor这些都表现不错 。
    > 2. 如果是没上产品页的，所有bot四大都可以，这种情况就看谁先开任务了。
    > 3. 如果是产品的库存和后台库存不同步，简称上上下下，加车或出现550错误，此时prism，balko，kylin比较比较好
    > 4. 如果是产品页和前端库存都上了，加车显示429，然后突然push库存的，这时候prism，kylin，whatbot表现不错。
- #### 普货的补货：
    > 1. 一点点上库存的，这种就完全是运气了，基本加车就能买到，推荐prism，kylin，valor。
    > 2. 首发之后产品页下了，突然上产品页和库存，这个时候whatbot，nsb，prism，kylin表现不错

## 四大其他补充
- ### 倒计时的release和随机release
  *倒计时的release是1卡1单，分站，也就是说张1卡站如果4个站都发同一款鞋的男女码，理论可以买8双。*  
  *随机release，也就是普货，不限购，一张卡买到底*

- ### 避免砍单
    四大砍单是由你profile的邮箱，地址以及卡号来决定的。
    > 四大对邮箱是没什么要求的，并没有什么所谓的xx邮箱比较好的说法，无论是gmail，还是qq邮箱，亦或者是域名邮箱等，都是没问题的。  
    > 地址有一定的要求，除开很少量必砍单的地址之外，大部分转运的地址都是不会砍单的，有些人可能会遇到1个人用的私人地址也砍单的情况，那一般是没鸡哥好。  
    > 卡的话没任何要求，visa，mc，ae，jcb都是支持的。  

    _大多数情况，不需要更改邮箱和卡的组合，只需要适当的修改addr1和addr2 即可，如addr1不鸡哥，addr2 使用随机鸡哥。_

- ### 发售和补货时的状态  
    > 1. 准点的发售，一般要提前开任务，一般在30-10分钟之间，具体最优时间是在网站上queue之前。  
    > 2. 如果遇到网站上了perma queue，可以暂时先停掉任务，节省流量，或者拉高delay也是可以的。  
    >3. 部分bot可以在发售前几分钟或者几十秒钟的时候强制启动任务来进行加车，推荐发售准点的时候拉点delay来获得更多加车进会。  
    > 4. 补货基本就只有加车429和加车550这两种情况，前者是有页面无库存。后者是页面库存和后台库存不同步。遇到429的时候基本可以关task了，应该是暂时没补或者补货结束，550就堆任务硬跑。
    