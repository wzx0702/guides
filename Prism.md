# Prism
## 总览  
老牌AIO bot，现在2.0支持绝大多数sneaker 网站以及超市等，UI好看，操作简单，优化非常好。  

- 控制面板：https://dashboard.prismaio.com/
- 续费价格：150$/3月
- 可随时解绑
  
### 1.控制面板
控制面板左侧会显示你绑定的email和dc账号。reset key既是machine deactivate，同时还支持绑定电话和2fa，同时支持window和macOS，下载link点击对应的操作系统图标即可。  
控制面板右侧显示的是你最近的数据，结账和失败数量，下方是快速任务执行框，需要设置site options，这个在后面会有描述
![](https://i.loli.net/2021/04/01/RmqUx6lNYCskjEc.jpg)  

### 2.主界面
下载安装完成之后，用你dashboard邮箱和密码登录bot就会看到主界面。
左边的tab栏分别是主界面，任务界面，profile界面，proxy界面和设置界面。  
左边的数据分析和dashboard里面是差不多的。右边有发售日历和最近结账，点击发售日历的detail可以快速创建相关网站的任务。  
右上角还有快捷键总览和快速任务按钮

![](https://i.loli.net/2021/04/01/D17gYMBKeqlimJf.jpg)
![](https://z3.ax1x.com/2021/04/01/cEiSn1.jpg)
![](https://z3.ax1x.com/2021/04/01/cEPj1J.jpg)

### 3.任务界面
Prism可以创建多个任务组,方便你来管理你的任务。每个任务组
对应一个网站。点击 “New Task Group”就会出来一个页面,你
需要填写弹窗内的信息来创建任务组。需要填写的信息包括:任务组名称
和任务组网站。
![](https://i.loli.net/2021/04/01/7R4emKViwXCAqnD.jpg)
在左边有任务组的基本信息,如果你想要换网站/任
务组名称可以点击左边右上方的 “...”。然后在 “Monitor Input”的
地方放入你这个任务组的 Keywords,如果跑 Shopify 可以做多组 KW,每组
KW 需要在 “()”里面,如: (+keywords,+1)(+keywords,+2)。而不同的
网站也有不同的 Monitor Input。然后接下来这个代理组,这个是你这个任务组的监控代理。


![](https://i.loli.net/2021/04/01/FHGWyczJstLoln4.jpg)  
点击 “+”就会出来一个创建任务的弹窗,每个不同类型的网站出来的界
面有些不同,但是都大同小异。在这里显示的是 Shopify 的创建任务界面。


![](https://i.loli.net/2021/04/01/XcImPqEsgMkCOzf.jpg)
### 4.profile界面  
添加或者导入profile之后，会看到如下图类似的卡片式profile列表。
![](https://i.loli.net/2021/04/01/aw3W5X1gfQm79FN.jpg)  
创建完成后在 Profile 的主界面,你可
以 CTRL + 左击选择多个 Profile,在点击 Group 就会出现创造 Profile
组的弹窗。输入你想要 Profile 组名称即可完成创建 Profile 组。  
![](https://i.loli.net/2021/04/01/dNJRxzW6teXpwgj.jpg)
### 5.proxy界面   
在proxies里面粘贴proxy并在name输入你想命名的proxy组名再点击create即可
![](https://i.loli.net/2021/04/01/V98ALtYo7hK2fDX.jpg)  


### 6.设置界面
- 通用设置，设置webhook和结账音效，下面两个是设置物理硬件联动，用不到  
![](https://i.loli.net/2021/04/01/Bax4jSQYmHALu6i.jpg)
- 站点选项，可以设置每个网站不同的默认选项，用来配合quick task的，即dashboard里面的quick task。这里可以指定每个网站的profile/profile group,尺码或者尺码范围，账号，代理和模式等等。  
![](https://i.loli.net/2021/04/01/RiHbGdXcQmSlyU2.jpg)
- 尺码组，可以设置各种不同的尺码范围，可以让你更快的创建任务。  
![](https://z3.ax1x.com/2021/04/01/cEi976.jpg)
- 邮费，shopify网站固定邮费，加速结账  
![](https://z3.ax1x.com/2021/04/01/cEPvc9.jpg)
- 账号，可以添加各种网站的账号，方便创建任务和设置默认quick task  
![](https://z3.ax1x.com/2021/04/01/cEPxXR.jpg)
- 解码器设置，prism有非常多重的解码器选项，点击右上角create即可添加。几种常用的可以优先添加，如capmaster，2cap，autosolver，shopify checkout/shopify checkpiont,fnl/jd,supreme和ys等。  
注意一下其中shopify checkout/checkpoint、supreme和ys都是需要登录google账号的。也就是说可以通过，只是需要在不同使用场景切换一下。而fnl/jd是不需要登录的，只需要放一条proxy进去即可。  
![](https://z3.ax1x.com/2021/04/01/cEip0x.jpg)



